#ifndef UTILS_QT_IRR_H
#define UTILS_QT_IRR_H

#include <irrlicht.h>
#include <QString>

QString PATH_TO_QSTRING(irr::io::path);
irr::io::path QSTRING_TO_PATH(QString str);



#endif // UTILS_QT_IRR_H
